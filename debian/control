Source: libm4rie
Section: math
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Felix Salfelder <felix@salfelder.org>,
           Cédric Boutillier <boutil@debian.org>,
           Julien Puydt <jpuydt@debian.org>
Homepage: https://github.com/malb/m4rie/
Build-Depends: debhelper-compat (= 12),
               libgivaro-dev,
               libgmp-dev,
               libm4ri-dev (>= 20140914~)
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/science-team/libm4rie.git
Vcs-Browser: https://salsa.debian.org/science-team/libm4rie
Rules-Requires-Root: no

Package: libm4rie-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libm4rie-0.0.20200125 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: extended Method of the Four Russians Inversion library, development files
 M4RIE is a library for fast arithmetic with dense matrices over small finite
 fields of even characteristic. It uses the M4RI library, implementing the same
 operations over the finite field F2.
 .
 The name M4RI comes from the first implemented algorithm: The "Method
 of the Four Russians" inversion algorithm.  This algorithm in turn is
 named after the "Method of the Four Russians" multiplication
 algorithm which is probably better referred to as Kronrod's method.
 .
 This package contains development files for the M4RIE library.

Package: libm4rie-0.0.20200125
Architecture: any
Section: libs
Pre-depends: ${misc:Pre-Depends}
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: extended Method of the Four Russians Inversion library, shared library
 M4RIE is a library for fast arithmetic with dense matrices over small finite
 fields of even characteristic. It uses the M4RI library, implementing the same
 operations over the finite field F2.
 .
 The name M4RI comes from the first implemented algorithm: The "Method
 of the Four Russians" inversion algorithm.  This algorithm in turn is
 named after the "Method of the Four Russians" multiplication
 algorithm which is probably better referred to as Kronrod's method.
 .
 This package contains the M4RIE shared library.
