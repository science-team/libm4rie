libm4rie (20250103-1) unstable; urgency=medium

  * Point to new upstream.
  * New upstream release.
  * Bump standards-version to 4.7.0.

 -- Julien Puydt <jpuydt@debian.org>  Thu, 16 Jan 2025 09:40:14 +0100

libm4rie (20200125-1) unstable; urgency=medium

  * New upstream release.
  * Bump std-ver to 4.5.0.

 -- Julien Puydt <jpuydt@debian.org>  Wed, 29 Jan 2020 09:45:46 +0100

libm4rie (20200115-1) unstable; urgency=medium

  * New upstream release.
  * Refresh packaging:
    - Remove d/compat and switch to dh-compat 12.
    - Bump std-ver to 4.4.1. 
    - Follow upstream soname bump. 
    - Ship the pkg-config file. 
    - Update d/copyright.

 -- Julien Puydt <jpuydt@debian.org>  Sun, 19 Jan 2020 09:39:28 +0100

libm4rie (20150908-2) unstable; urgency=medium

  * Refresh packaging:
    - Use my debian.org mail address.
    - Point Vcs-* fields to salsa. 
    - Bump d/watch to version 4. 
    - Mark the -dev package Multi-Arch: same. 
    - Bump dh compat to 11. 
    - Bump std-ver to 4.1.5. 

 -- Julien Puydt <jpuydt@debian.org>  Tue, 07 Aug 2018 10:56:09 +0200

libm4rie (20150908-1) unstable; urgency=medium

  * Team upload.

  [Andreas Tille]

  * Don't use bibtex syntax in yaml file.

  [Julien Puydt]

  * New upstream version 20150908.
  * Pushed standards-version up.
  * Updated Vcs-* fields.
  * Updated d/control and d/watch to new upstream location.
  * Updated d/upstream/metadata to what upstream declares.
  * Added myself to uploaders.
  * Remove the manual -dbg package.
  * Simplified d/rules.
  * Enabled parallel builds.

 -- Julien Puydt <julien.puydt@laposte.net>  Sat, 21 May 2016 21:52:03 +0200

libm4rie (20140914-1) unstable; urgency=medium

  * Imported Upstream version 20140914
  * Bump Standards-Version to 3.9.6 (no changes needed)
  * Don't predepend directly on multiarch-support
  * Fix URL in debian/watch
  * Use now upstream tarball instead of bitbucket repository; remove gbp.conf
  * Remove README.upstream, as it didn't correspond to the actual workflow
  * Rename packages with the new soname
  * Determine from changelog the name of -dbg package in debian/rules
  * Clean copyright file from paragraphs about removed files
  * Move upstream data to debian/upstream/metadata

 -- Cédric Boutillier <boutil@debian.org>  Sat, 18 Oct 2014 21:53:26 +0200

libm4rie (20130416-2) unstable; urgency=low

  * Upload to unstable.
  * Add debian/upstream file.

 -- Cédric Boutillier <boutil@debian.org>  Mon, 17 Jun 2013 22:02:33 +0200

libm4rie (20130416-1) experimental; urgency=low

  * New upstream version
  * Bump Standards-Version: to 3.9.4 (no changes needed)
  * Add Vcs-* fields 
  * Produce a debug package

 -- Cédric Boutillier <boutil@debian.org>  Thu, 28 Mar 2013 15:07:13 +0100

libm4rie (20120613-1) experimental; urgency=low

  * Initial release. (Closes: #697792)
  * Override lintian message about the absence of upstream changelog.

 -- Cédric Boutillier <boutil@debian.org>  Mon, 07 Jan 2013 19:27:18 +0100
